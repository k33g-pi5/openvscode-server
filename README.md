# OpenVSCode server template

```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:k33g-pi5/openvscode-server.git
git add .
git commit -m "🎉 Initial commit"
git push --set-upstream origin main
```

## How to

```bash
git clone https://gitlab.com/k33g-pi5/openvscode-server.git
cd openvscode-server
docker compose build # if you change something into docker-dev-environment/Dockerfile
docker compose up
```

> url: http://localhost:3000/?tkn=ilovepandas&folder=/home/workspace

## Start something at boot (faking k33g user)

```bash
sudo nano /etc/rc.local
# add
cd /home/k33g/openvscode-server
su k33g -c 'docker compose up'
# do not remove `exit 0` at the end
sudo chmod +x /etc/rc.local
```
